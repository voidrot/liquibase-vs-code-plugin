# Change Log

All notable changes to the "liquibase" extension will be documented in this file.

## v0.1.1

No functionality changes for this release

- Update to Changelog
- Added Extension Icon for marketplace

## v0.1.0

- Initial release
- Initial Support for all community change types
