# Liquibase Support (YAML)

This extension provides syntax, snippet, and command support for using liquibase with yaml files.

All snippets are prefixed with the word `liquibase` to make sure that they wont collide with other snippets.

Snippets are also able to be called with short hand syntax such as `lcdc` or `cdc` refer to `liquibase-create-database-change`

Snippet descriptions are copied from the liquibase docs.

## Features

### Active Features

- Snippets for Liquibase Community Change Types

### Planned

- Partial Nested Snippets (Column, Constraints, etc)
- Initialize Basic YAML Liquibase project
- Support for running liquibase cli commands

<!-- ## Requirements

- liquibase cli installed into PATH -->

## Known Issues

None at this time

## Release Notes

## v0.1.1

No functionality changes for this release

- Update to Changelog
- Added Extension Icon for marketplace

## v0.1.0

- Initial release
- Initial Support for all community change types
